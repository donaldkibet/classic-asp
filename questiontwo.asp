﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Hello ASP</title>
</head>
<body>
<!-- Question Two Answer -->
<div>
<!--    &nbsp is a html special character for printing spaces
        The first outer loop is for printing the rows 
        The second inner loop is for printing the space before the * character
        The Third inner loop is for printing the * symbol
 -->
  <%
    Dim space :  space = 1
    Dim rowNumbers: rowNumbers = 4
    For i = 1 To rowNumbers
        For j = 1 To (rowNumbers - i)
            Response.Write(" &nbsp")
        Next
        For k = 1 to (2 * i - 1)
            Response.Write("*")
        Next
        Response.Write("<br/>")
    Next
  %>
  <% 
    For i = 1 to rowNumbers
        For j = 1 to (rowNumbers)
            Response.Write("&nbsp")
        Next
        Response.Write("*" & "<br/>")
    Next
   %>
</div>
<hr />   
</body>
</html>